package br.com.mastertech.usuariocep.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "enderecos")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private LocalDate dataConsulta;

    @OneToOne(cascade=CascadeType.REFRESH)
    @JoinColumn(name = "cep")
    private Cep cep;

    public Usuario() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDataConsulta() {
        return dataConsulta;
    }

    public void setDataConsulta(LocalDate dataConsulta) {
        this.dataConsulta = dataConsulta;
    }

    public Cep getCep() {
        return cep;
    }

    public void setCep(Cep cep) {
        this.cep = cep;
    }
}
