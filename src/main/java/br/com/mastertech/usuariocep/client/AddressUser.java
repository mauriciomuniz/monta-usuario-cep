package br.com.mastertech.usuariocep.client;

import br.com.mastertech.usuariocep.model.Cep;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "endereco")
public interface AddressUser {

    @GetMapping("/cep/{cep}")
    @NewSpan(name = "endereco-client")
    Cep consultaCEP(@PathVariable(name = "cep") String cep);
}
