package br.com.mastertech.usuariocep.service;

import br.com.mastertech.usuariocep.DTO.CadastroUsuario;
import br.com.mastertech.usuariocep.client.AddressUser;
import br.com.mastertech.usuariocep.model.Cep;
import br.com.mastertech.usuariocep.model.Usuario;
import br.com.mastertech.usuariocep.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class UsuarioService {

    @Autowired
    private AddressUser addressUser;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private CepService cepService;

    public Usuario cadastrarUsuario(CadastroUsuario cadastroUsuario) {
        Cep retornoCep = addressUser.consultaCEP(cadastroUsuario.getCep());
        cepService.salvaCep(retornoCep);
        return montaUsuario(retornoCep);
    }

    public Usuario montaUsuario (Cep cep) {
        Usuario usuario = new Usuario();
        LocalDate date = LocalDate.now();
        usuario.setCep(cep);
        usuario.setDataConsulta(date);
        usuario = usuarioRepository.save(usuario);
        return usuario;
    }
}
