package br.com.mastertech.usuariocep.service;

import br.com.mastertech.usuariocep.model.Cep;
import br.com.mastertech.usuariocep.repository.CepRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    private CepRepository cepRepository;

    public Cep salvaCep (Cep cep) {
        return cepRepository.save(cep);
    }
}
