package br.com.mastertech.usuariocep.controller;

import br.com.mastertech.usuariocep.DTO.CadastroUsuario;
import br.com.mastertech.usuariocep.model.Usuario;
import br.com.mastertech.usuariocep.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public Usuario consultaEndereco(@RequestBody @Valid CadastroUsuario cadastroUsuario) {
        return usuarioService.cadastrarUsuario(cadastroUsuario);
    }
}
