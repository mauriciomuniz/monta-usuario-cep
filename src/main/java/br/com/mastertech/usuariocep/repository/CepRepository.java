package br.com.mastertech.usuariocep.repository;

import br.com.mastertech.usuariocep.model.Cep;
import org.springframework.data.repository.CrudRepository;

public interface CepRepository extends CrudRepository<Cep, Integer> {
}
