package br.com.mastertech.usuariocep.repository;

import br.com.mastertech.usuariocep.model.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
}
